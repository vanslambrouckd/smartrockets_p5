function Rocket(dna) {
    this.col = color(random(0, 255),random(0,255),random(0,255));

	this.pos = createVector(width/2, height);
    this.accel = createVector();
    this.vel = createVector();
    this.completed = false;
    this.crashed = false;

    if (dna) {
        this.dna = dna;
    } else {
        this.dna =  new DNA();
    }
    this.fitness = 0;

    this.applyForce = function(force) {
        this.accel.add(force);
    }

    this.update = function() {
        var d = dist(this.pos.x, this.pos.y, target.x, target.y);
        // if (d < 100) {
        //     console.log(this.fitness*10);
        // }
        if (d < 10) {
            console.log('completed');
            this.completed = true;
            this.pos = target.copy();
        }

        if (this.pos.x > width || this.pos.x < 0) {
            this.crashed = true;
        }

        if (this.pos.y > height || this.pos.y < 0) {
            this.crashed = true;
        }
        this.applyForce(this.dna.genes[count]);

        if (!this.completed && !this.crashed) {
            this.vel.add(this.accel);
            this.pos.add(this.vel);
            this.accel.mult(0);
            this.vel.limit(4);
        }
    }

    this.draw = function() {
        /*
        push & pop:
        zorgen dat de transformaties enkel van toepassing zijn op hetgene die tussen push en pop staan
        */

        push();
        // noStroke();

        this.calcFitness();
        //console.log(this.fitness);
        var max_weight = dist(canvasW,canvasH, target.x, target.y);
        var weight = map(this.fitness, 0, 1, 0, 255);
        strokeWeight(weight);
        stroke(255);
        // fill(red(this.col), green(this.col), blue(this.col), alpha);
        // var alpha = map(this.fitness, 0, 1, 0, 255);
        // console.log(alpha);
        // console.log(this.fitness);
        fill(255,0,0, 0);
        translate(this.pos.x, this.pos.y);
        rotate(this.vel.heading()); //heading: angle of rotation van de vector
        rectMode(CENTER);
        rect(0,0, 50, 10);
        pop();
    }

    this.calcFitness = function() {
        /*
        de rockets die dichter bij de target geraken, krijgen hogere fitness
        distance van rocket tot target meten via dist function
        distance normalizeren: 1 / distance
        */
        var d = dist(this.pos.x, this.pos.y, target.x, target.y);
        this.fitness = 1 / d; //normalize
        //this.fitness = map(d, 0, width, width, 0);

        if (this.crashed) {
            this.fitness /= 10;
        }

        if (this.completed) {
            this.fitness *= 10;
        }
    }
}