function DNA(genes) {
    this.mag = 1;
    if (genes) {
        this.genes = genes;
    } else {
        this.genes = [];
        for (var i = 0; i < lifespan; i++) {
            this.genes[i] = p5.Vector.random2D();
            this.genes[i].setMag(this.mag);
        }
    }

    this.crossover = function(partner) {
        /*
        de crossover functie gaat de dna van 2 rockets samenvoegen
        (een deel van dna van rocket 1 + deel van dna van rocket 2)

        */
        var newgenes = []
        var mid = floor(random(0, this.genes.length));

        for (var i = 0; i < this.genes.length; i++) {
            if (i > mid) {
                newgenes[i] = this.genes[i];
            } else {
                newgenes[i] = partner.genes[i];
            }
        }

        return new DNA(newgenes);
    }

    this.mutation = function() {
        for (var i = 0; i < this.genes.length; i++) {
            if (random(1) < 0.01) {
                this.genes[i] = p5.Vector.random2D();
                this.genes[i].setMag(this.mag);
            }
        }
    }
}