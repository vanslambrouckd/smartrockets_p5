function Population() {
    this.rockets = [];
    this.popsize = 25;
    this.matingpool = [];

    for (var i = 0; i < this.popsize; i++) {
        this.rockets[i] = new Rocket();
    }

    this.evaluate = function() {
        /*
        de rockets met de hoogste fitness, 
        moeten bij de volgende generatie meer kans krijgen om nieuwe rockets te genereren (matingpool)
        deze komen dus meerdere keren voor in de matingpool array
        */

        /*
        A) de fitness waardes normalizeren:
        1) max fitness ophalen (de maximumwaarde van alle rockets samen)
        2) alle fitness waardes overlopen en delen door de max fitness

        B) de rocket X aantal keer toevoegen adhv van zijn fitness
        (hogere fitness:
        => rocket komt meerdere keer voor in matingpool 
        => meer kans om geselecteerd te worden bij volgende generatie)
        */

        //stap 1
        var maxFitness = 0;
        for (var i = 0; i < this.popsize; i++) {
            this.rockets[i].calcFitness();
            if (this.rockets[i].fitness > maxFitness) {
                maxFitness = this.rockets[i].fitness;
            }
        }

        //stap 2
        for (var i = 0; i < this.popsize; i++) {
            this.rockets[i].fitness /= maxFitness;
            // console.log("fitness "+i+": "+this.rockets[i].fitness);
        }
        
        //stap3:
        this.matingpool = [];
        for (var i = 0; i < this.popsize; i++) {
            var n = this.rockets[i].fitness*100;
            for (var j = 0; j < n; j++) {
                this.matingpool.push(this.rockets[i]);
            }
        }
    }

    this.selection = function() {
        var newRockets = [];
        // console.log("matingpool size:"+this.matingpool.length);
        for (var i = 0; i < this.rockets.length; i++) {
            var parentA = random(this.matingpool).dna;
            var parentB = random(this.matingpool).dna;
            var childdna = parentA.crossover(parentB);
            childdna.mutation();
            newRockets[i] = new Rocket(childdna);
        }
        this.rockets = newRockets;
    }

    this.run = function() {
        for (var i = this.rockets.length-1; i >= 0; i--) {
            this.rockets[i].update();
            this.rockets[i].draw();
            // if (this.rockets[i].completed) {
            //     this.rockets.splice(i, 1);
            //     this.popsize -= 1;
            // }
        }
    }

    this.getRunningRockets = function() {
        var nr = 0;
        for (var i = 0; i < this.rockets.length; i++) {
            if (!this.rockets[i].crashed && !this.rockets[i].completed) {
                nr++;
            }
        }
        return nr;
    }
}