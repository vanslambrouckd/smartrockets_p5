//doordoen vanaf 35:34
//https://codepen.io/shiffman/pen/XKYbLP
//https://www.youtube.com/watch?v=bGz7mv2vD6g
var population;
var lifespan = 600; //gene lifespan
var lifeP;
var count = 0; 
var target;
var t; 
var canvasW = 500;
var canvasH = 600;
function setup() {
	createCanvas(canvasW,canvasH);
    population = new Population();
    lifeP = createDiv();
    lifeP.style("color", "#fff");
    lifeP.position(10, 10);
    target = createVector(width/2, height/4.5);
}

function draw() {
    background(0);
    population.run();
    count++;

    if ( (count == lifespan) || (population.getRunningRockets() == 0) ) {
        population.evaluate();
        population.selection();
        count = 0;
    }
    // console.log(population.matingpool.length);
    lifeP.html(count);
    ellipse(target.x, target.y, 20, 20);
}